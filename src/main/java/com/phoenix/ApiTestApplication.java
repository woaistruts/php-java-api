package com.phoenix;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@SpringBootApplication
public class ApiTestApplication {

	public static void main(String[] args){
		ConfigurableApplicationContext applicationContext = SpringApplication.run(ApiTestApplication.class, args);
		BannerService bannerService = applicationContext.getBean(BannerService.class);
		bannerService.init();
	}

	@RestController
	@RequestMapping("")
	class BannerAction{

		@Autowired
		private BannerService bannerService;

		@RequestMapping("/all")
		public List<Banner> all(){
			return bannerService.getAll();
		}
	}
	@Service
	public class BannerService{

		@Autowired
		private BannerDao bannerDao;

		public void init(){
			Banner b = new Banner();
			b.setUrl("/res/img/h4-slide.png");
			b.setDes("<h2 class='caption title'>" +
					"                        iPhone <span class='primary'>6 <strong>Plus</strong></span>" +
					"                    </h2>" +
					"                    <h4 class='caption subtitle'>Dual SIM</h4>" +
					"                    <a class='caption button-radius' href='#'><span class='icon'></span>Shop now</a>");
			Banner b1 = new Banner();
			b1.setUrl("/res/img/h4-slide2.png");
			b1.setDes("<h2 class='caption title'>" +
					"                        by one, get one <span class='primary'>50% <strong>off</strong></span>" +
					"                    </h2>" +
					"                    <h4 class='caption subtitle'>school supplies & backpacks.*</h4>" +
					"                    <a class='caption button-radius' href='#'><span class='icon'></span>Shop now</a>");
			Banner b2 = new Banner();
			b2.setUrl("/res/img/h4-slide3.png");
			b2.setDes("<h2 class='caption title'>" +
					"                        Apple <span class='primary'>Store <strong>Ipod</strong></span>" +
					"                    </h2>" +
					"                    <h4 class='caption subtitle'>Select Item</h4>" +
					"                    <a class='caption button-radius' href='#'><span class='icon'></span>Shop now</a>");
			Banner b3 = new Banner();
			b3.setUrl("/res/img/h4-slide4.png");
			b3.setDes("<h2 class='caption title'>" +
					"                        by one, get one <span class='primary'>50% <strong>off</strong></span>" +
					"                    </h2>" +
					"                    <h4 class='caption subtitle'>school supplies & backpacks.*</h4>" +
					"                    <a class='caption button-radius' href='#'><span class='icon'></span>Shop now</a>");
			bannerDao.save(b);
			bannerDao.save(b1);
			bannerDao.save(b2);
			bannerDao.save(b3);
		}

		public List<Banner> getAll(){
			return bannerDao.findAll();
		}
	}
}
interface BannerDao extends JpaRepository<Banner, Integer> {

}
@Entity
@Table(name = "banner")
class Banner implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String url;

	@Column(name = "des",length = 1000)
	private String des;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDes() {
		return des;
	}

	public void setDes(String des) {
		this.des = des;
	}

	@Override
	public String toString() {
		return "Banner{" +
				"id=" + id +
				", url='" + url + '\'' +
				", des='" + des + '\'' +
				'}';
	}
}